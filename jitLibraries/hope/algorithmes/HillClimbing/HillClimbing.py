import random
import copy
from hope import jit 

random.seed()

@jit
def testAssignments(cnf, myAssign):
    goodClause = 0
    s=len(cnf)
    for dd in range(s):
        clause = cnf[dd]    
        for i in range(len(myAssign)):
            if myAssign[i]*clause[i] > 0:
                goodClause += 1
                break
    return goodClause


def GenerateRandomAssignment(nbvar):
    boolvec = []
    for _ in range(nbvar):
        boolvec.append(random.randrange(-1, 2, 2))
    return boolvec

@jit
def DeepCopy(CopyFrom):
    return copy.deepcopy(CopyFrom)

@jit
def GetBestAssignment(cnf, assignment):
    BestAssign = DeepCopy(assignment)
    BestC = testAssignments(cnf, assignment)
    for i in range(len(assignment)):
        # change node
        assignment[i] *= -1
        tempC = testAssignments(cnf, assignment)
        if(tempC > BestC):
            BestC = tempC
            BestAssign = DeepCopy(assignment)
        # reverse node change, continue loop through with next node
        assignment[i] *= -1

    return BestAssign, BestC

@jit
def HillClimbing(cnf, assignment):
    CurrentEval = testAssignments(cnf, assignment)
    NextAssignment, NextEval = GetBestAssignment(cnf, assignment)
    if(NextEval <= CurrentEval):
        # print(CurrentEval)
        return CurrentEval
    else:
        # print("dig")
        return HillClimbing(cnf, NextAssignment)

@jit
def Solve(cnf, s):
    AveC = []
    HighC = -1
    for _ in range(s):
        Best = -1
        # print("restart")
        MyAssignment = GenerateRandomAssignment(len(cnf[0]))
        newC = HillClimbing(cnf, MyAssignment)
        if newC > Best:
            Best = newC
            if Best > HighC:
                HighC = Best
            if(Best == len(cnf)):
                print("Satisfiable")
                AveC.append(Best)
                return sum(AveC)/len(AveC), HighC
        AveC.append(Best)
    return sum(AveC)/len(AveC), HighC


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def cnfExtractor(filename):
    with open(filename) as file:
        cnf = []
        nbclauses = -1
        nbvar = -1
        content = file.readlines()
        curClause = -1
        for line in content:
            splitLine = line.split(' ')
            if '' in splitLine : 
                splitLine.remove('')
            if splitLine[0] == 'p' and splitLine[1] == 'cnf':
                nbvar = int(splitLine[2])
                nbclauses = int(splitLine[3])
                # print(nbvar)
                # print(nbclauses)
                #print("previous two nbvar, nbclauses")
                cnf = [[0 for i in range(nbvar)] for j in range(nbclauses)]
            elif nbvar > 0 and nbclauses > 0 and splitLine[0] != 'c':
                curClause += 1
                for variable in splitLine:
                    if is_int(variable):
                        if int(variable) != 0:
                            cnf[curClause][abs(
                                int(variable))-1] = int(variable)
        return cnf


if __name__ == '__main__':
    niters = 10
    n = 10
    fname = "100.410.18944532.cnf"
    cnf = cnfExtractor("/algorithmes/cnfFiles/"+fname)
    for _ in range(niters):
        # Solve(cnf, n)
        # HillStart = time.time()
        cHillAve, HighHillc = Solve(cnf, n)
        # HillEnd = time.time()
        print("Ave case Hill Climbing:", cHillAve, "Highest:", HighHillc)
        # print("Hill Climbing run time:", HillEnd - HillStart, "seconds")

