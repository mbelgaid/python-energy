from multiprocessing import Pool #,dummy
import random
import copy
from numba import jit 
random.seed()

@jit
def testAssignments(cnf, myAssign):
    goodClause = 0
    for clause in cnf:
        for i in range(len(myAssign)):
            if myAssign[i]*clause[i]>0:
                goodClause+=1
                break
    return goodClause
@jit
def GenerateRandomAssignment(nbvar):
    boolvec = []
    for _ in range(nbvar):
        boolvec.append(random.randrange(-1,2,2))
    return boolvec
@jit
def DeepCopy(CopyFrom):
    return copy.deepcopy(CopyFrom)


class testAssignment(object):
    def __init__(self,cnf,assignment):
        self._cnf = cnf
        self._assignment = assignment
    
    def __call__(self,index):
        tempAssignment = DeepCopy(self._assignment)
        tempAssignment[index] *= -1
        return tempAssignment, testAssignments(self._cnf,tempAssignment)

@jit
def GetBestAssignment(cnf, assignment):
    
    

    BestAssign = DeepCopy(assignment)
    BestC = testAssignments(cnf,assignment)
    pool = Pool()
    dd = testAssignment(cnf,assignment)
    #l = [ testAssignment(cnf,assignment,i) for i in range(len(assignment)) ]
    l = pool.map(dd,range(len(assignment)))
    for assign,count in l : 
        if count > BestC : 
            BestC = count
            BestAssign = assign
    pool.close()
    pool.join()
    #reverse node change, continue loop through with next node
    # assignment[i] *= -1

    return BestAssign, BestC

@jit
def HillClimbing(cnf, assignment):
    CurrentEval = testAssignments(cnf, assignment)
    NextAssignment, NextEval = GetBestAssignmentseq(cnf, assignment)
    if(NextEval<=CurrentEval):
        #print(CurrentEval)
        return CurrentEval
    else:
        #print("dig")
        return HillClimbing(cnf, NextAssignment)



class IsntantSolever(object):
    def __init__(self,cnf):
        self._cnf=cnf 
    
    def __call__(self,i):
        MyAssignment = GenerateRandomAssignment(len(self._cnf[0]))
        return  HillClimbing(self._cnf, MyAssignment)
        
@jit
def GetBestAssignmentseq(cnf, assignment):
    BestAssign = DeepCopy(assignment)
    BestC = testAssignments(cnf,assignment)
    for i in range(len(assignment)):
        #change node
        assignment[i] *= -1
        tempC = testAssignments(cnf,assignment)
        if(tempC > BestC):
            BestC = tempC
            BestAssign = DeepCopy(assignment)
        #reverse node change, continue loop through with next node
        assignment[i] *= -1

    return BestAssign, BestC

@jit
def Solve(cnf, s):
    pool = Pool()
    isntantSolever = IsntantSolever(cnf)
    l= pool.map(isntantSolever,range(s))
    pool.close()
    pool.join()
    return sum(l)/len(l), max(l)

def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def cnfExtractor(filename ):
    with open(filename) as file:
        cnf = []
        nbclauses = -1
        nbvar = -1
        content = file.readlines()
        curClause = -1
        for line in content:
            splitLine = line.split(' ')
            if splitLine[0] == 'p' and splitLine[1] == 'cnf':
                nbvar = int(splitLine[2])
                nbclauses = int(splitLine[3])
                #print(nbvar)
                #print(nbclauses)
                #print("previous two nbvar, nbclauses")
                cnf = [[0 for i in range(nbvar)] for j in range(nbclauses)]
            elif nbvar > 0 and nbclauses > 0 and splitLine[0] != 'c':
                curClause += 1
                for variable in splitLine:
                    if is_int(variable):
                        if int(variable) != 0:
                            cnf[curClause][abs(int(variable))-1] = int(variable)
        return cnf

if __name__=='__main__':
    niters = 10 
    n= 10
    fname ="100.460.90785379.cnf" 
    cnf = cnfExtractor("/algorithmes/cnfFiles/"+fname)
    for _ in range(niters) : 
        cHillAve, HighHillc = Solve(cnf, n)
        # HillEnd = time.time()
        print("Ave case Hill Climbing:", cHillAve, "Highest:", HighHillc)